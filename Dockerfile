FROM python:3.10

ADD requirements.txt requirements.txt
ADD src/ src/

RUN pip install -r requirements.txt

WORKDIR src/

CMD ["python", "main.py"]
