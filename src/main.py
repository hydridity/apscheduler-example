from apscheduler.schedulers.background import BlockingScheduler

import requests
from datetime import datetime
import os
import socket


def send_message():
    message = "Je {} posielam 5 minutove kekw z {}".format(datetime.now().ctime(), socket.gethostname())
    requests.post(url="https://discord.com/api/webhooks/1076190541793988630/Q1u9FDD1zqU3-mUTV_1OAUp4O6-O6xVk7zmVRRfTTntj8kjBajeWQErQBlES9iKPzc65"
                  , data={"content": message}) #Toto nema nic s discord botmi,  to vola len rest api endpoint (webhook), ktory discord preklada do message
    # tu by sa dal dat try block ktory by nieco robil ak by request zlyhal a zaroven ten asp scheduler ma error handling ak funkcia vrati error
    print("Sending kekw request")


if __name__ == '__main__':
    scheduler = BlockingScheduler()
    scheduler.add_job(send_message, 'interval', minutes=5)
    print('I\'m running press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))

    try: # toto funguje alebo nefunguje podla os a cmd promptu
        scheduler.start()
    except (KeyboardInterrupt, SystemExit):
        pass
